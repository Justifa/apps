/****************************************************************************
 * examples/as726x/as726x_main.c
 *
 *   Copyright (C) 2019 Fabian Justi. All rights reserved.
 *   Author: Fabian Justi <fabianjusti@gmx.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <nuttx/sensors/as726x.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * as726x_main
 ****************************************************************************/

#ifdef BUILD_MODULE
int main(int argc, FAR char *argv[])
#else
int as726x_main(int argc, char *argv[])
#endif
{

  struct as726x_sensor_data_s data;
  int fd;
  int i;

  printf("as726x app is running.\n");

  fd = open("/dev/spectr0", O_RDWR);

  for (i = 0; i < 20; i++)
    {
      read(fd, &data, sizeof(struct as726x_sensor_data_s));

      //printf for as7262
      //printf("Read spectral values for 450nm, 500nm, 550nm, 570nm, 600nm and 650nm .\n");
      //printf("Violet = %04d , Blue = %04d ,  Green = %04d , Yellow = %04d , Orange = %04d , Red = %04d \n",
      //        data.v_r_value, data.b_s_value, data.g_t_value, data.y_u_value, data.o_v_value, data.r_w_value);

      //printf for as7263
      printf("Read spectral values for 610nm, 680nm, 730nm, 760nm, 810nm and 860nm .\n");
      printf("Channel R = %04d , Channel S = %04d ,  Channel T = %04d , Channel U = %04d , Channel V = %04d , Channel W = %04d \n",
              data.v_r_value, data.b_s_value, data.g_t_value, data.y_u_value, data.o_v_value, data.r_w_value);

      usleep(50000);
    }

  close(fd);
  return 0;
}
